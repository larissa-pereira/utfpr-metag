import static org.junit.Assert.*;

import org.junit.Test;

public class PrincipalTest {

	@Test
	public void valorMedioTest() {
		int[] array = new int[] {1,2,3,4,5};
		assertEquals(3, Principal.calcularMedia(array), 0.1);
	}

}
